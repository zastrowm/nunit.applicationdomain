This project has moved to [GitHub](https://github.com/zastrowm/NUnit.ApplicationDomain).  Please navigate to https://github.com/zastrowm/NUnit.ApplicationDomain.
 
If you're looking for the original bitbucket repository, it is now loacated at https://bitbucket.org/zastrowm/nunit.applicationdomain_bitbucket